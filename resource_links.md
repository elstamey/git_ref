#Resource Links

These are the many links I've collected and want to save for my project research.

- From Github (thanks to Elizabeth Naramore)
	
	- [training kits](https://training.github.com/kit/) 
	
	- [video training](https://training.github.com/resources/videos/)
	
	- [training guides](https://guides.github.com/)
	
	  

- From Emma Jane who is developing a training course for OSCON 2014
	
	- [gitforteams](https://github.com/emmajane/gitforteams)
		
		This repo hosts two slide decks and some other resources that emmajane has collected.
		
	- [developer workflow](http://developerworkflow.com/) 
	
		This is a site in progress.  I believe she will have cultivated a lot of training resources by the time of 