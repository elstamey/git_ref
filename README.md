These are general purpose git commands to get you started.  We are working from an existing project directory -> staging those files with git -> then pushing the changes to [github enterprise]

You may also use the [git ref]

If you have installed the git app, from the GUI, choose Preferences -> add the comand-line tools

###Configure the Global values.  These global settings are put into your ~/.gitconfig

	> git config --global user.name "name"	
	
	> git config --global user.email "email@domain.com"

	Color the UI which makes the log info more readable.
	
	> git config --global color.ui auto
	
###Create a .gitignore file (some suggestions depending on OS)

	# OS files
	Thumbs.db
	ehthumbs.db
	Desktop.ini
	$RECYCLE.BIN/
	.*
	!.gitignore
	!.htaccess
	*~
	Icon
	
	# Dreamwever Files
	*.LCK
	_devnotes/*
	_notes/ 
	
	#Eclipse/Aptana Files
	.project/
	

###Initialize the Repo

	> cd proj_dir
	
	> git init
	
	> git add .
	
	> git commit -m "Initial commit"
	
###Create your Repo at [github enterprise]

	> git remote add origin {URL}
	
	> git push -u origin master
	the -u tells it to track origin/master
	
	
###General purpose commands


#####Add all files (slight variants) to the stage


	> git add . 
	looks at the working tree and adds all those paths that are either changes, new, and not ignored to the staged changes 
	
	> git add -u
	looks at the currently tracked changes and stages thechanges to those files if they are differentor if they have been removed
	
	> git add -A 
	does both
	
	Add an individual file
	>git add filename
	 
#####Remove files from the stage

	> git rm filename
	removes the file from the stage as well as deletes it.
	
	> git rm --cached filename
	remove the file from staging without deleting the actual file
	
#####Rename a file/Move the file

	> git mv filename_old filename_new	
	> git mv filename dir/filename
	> git add -A
	
	or
	
	> mv filename_old filename_new
	> git add -A 
	(git add .) won't work because it won't include the removal

#####Commit the changes

	> git commit -m "commit message details, should be specific"
	
	> git commit -am "commit message details, should be specific"
	This basically tells Git to run git add on any file that is "tracked" - that is, any file that was in your last commit and has been modified. 
	
#####Status/Diff

	> git status -s
	See the status of your git staging area compared to the code in your working area.  
	-s shortens the output
	
	> git diff
	Describe the changes that are either staged or modified on disk but unstaged.
	
	> git diff --cached
	Shows what contents have been staged.  This is what will go into the next commit snapshot.
	
	> git diff HEAD
	Shows both stages and unstaged changes, the difference between your working directory and the last commit, ignoring the staging area.
	
	> git diff --stat
	A summary of changes. This provides less than the full diff output but more than status.
	
#####Undo changes

	> git reset --soft
	This undoes the last commit and puts the files back onto the stage

**warning: if you use --hard, any changes not in the index or that have not been committed will be lost**

	> git reset --hard
	This undoes the last commit and unstages those files.  
	
#####Stash

	> git stash
	Takes the current working directory and index and puts it aside for later.  Gives you a clean working directory without losing the changes you were making.  Your working directory will be at the state of the last commit.
	
	> git stash list
	Shows what you've stowed on the stash
	
	> git stash apply
	When you are ready to resume working, this command will bring back the saved changes to the working directory.
	
	> git stash pop 
	Removes the item from the stack while bringing it to the working directory
	
	> git stash drop stash@{1}
	Use this command when you are done with the stash or want to remove it from the list. 

**only use this command if you are SURE you're done with the stash**

	> git stash clear
	Removes all of the stored items
	
	
[github enterprise]:http://github.ncsu.edu
[git ref]:http://gitref.org/basic
