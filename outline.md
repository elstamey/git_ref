Presentation Outline

- Usesfor Github
	- NO MORE .backup .save _backup/ etc!!!!
	- Refactoring exsiting code base without loss of knowledge
	- Teamwork regardless of location
	- Code sharing/Idea sharing
		- can collaborate with developers anywhere, control their access
	- Issue reporting / discussion
	- Task Tracking
	- Documenting code alongside the codebase (wikis/readme) 
	- Can keep your projects organized while switching between projects
	
- Obstacles
	- Vulnerability/Sensitivity 
	- Not comfortable with securing the code base
	
- Tutorial
	- Configure/set up a repo
	- Adding files/commit/rollback
	- branching 
