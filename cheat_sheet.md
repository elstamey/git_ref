##Overwrite from Origin

	$ git fetch --all
	$ git reset --hard origin/master

##Undo a commit and redo

	$ git commit ...              (1)
	$ git reset --soft 'HEAD^'    (2)
	$ edit                        (3)
	$ git add ....                (4)
	$ git commit -c ORIG_HEAD     (5)
	
##Remove commits from the history

- Checkout the last usable commit.
- Create a new branch to work on.
- cherry pick commit 3
- Cherry pick commit 1.
- Checkout master.
- Reset master to last usable commit.
- Merge our new branch onto master.
- Push master to the remote repo.



		$ git checkout <hash-start>
		$ git checkout -b fixer 
		$ git cherry-pick <hash+3>
		$ git cherry-pick <hash+1>
		$ git checkout master 
		$ git reset --hard <hash-start> 
		$ git merge fixer 
		$ git push --force origin master 
		
##Remove part of a change to a file

	$ git checkout -p filename
	
###Commit only a hunk of a file change

	$ git add -p filename
	
It'll ask you what you want to stage. You can then:

	s - split whatever change into smaller hunks. This only works if there is at least one unchanged line in the "middle" of the hunk, which is where the hunk will be split

	then either:
	y to stage that hunk, or
	n to not stage that hunk, or
	s to split the current hunk into smaller pieces
	e to manually edit the hunk (useful when git can't split it automatically)
	d to exit or go to the next file.
	
	? to get the whole list of available options.

	If the file is not in the repository yet, do first git add -N filename.x. Afterwards you can go on with git add -p filename.x.
		
		
### Prune branches

	> git fetch origin --prune

### Add files to the previous commit

	> git commit --amend –C HEAD
