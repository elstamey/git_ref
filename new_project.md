###Configure the Global values.  These global settings are put into your ~/.gitconfig

	> git config --global user.name "name"	
	
	> git config --global user.email "email@domain.com"

	Color the UI which makes the log info more readable.
	
	> git config --global color.ui auto

## Create a .gitignore file
There are some possible files to exclude using the .gitignore

	# OS files
	Thumbs.db
	ehthumbs.db
	Desktop.ini
	$RECYCLE.BIN/
	.*
	!.gitignore
	!.htaccess
	*~
	Icon
	
	# Dreamwever Files
	*.LCK
	_devnotes/*
	_notes/ 
	
	#Eclipse/Aptana Files
	.project/


## Protect Passwords

Before initializing a new project in Git, verify where the passwords and keys are included in the code.

- Copy the file to a backup file

		> cp filename filename.default

- add the original file to the .gitignore 

- Remove the passwords from the backup file
		
**If the .htaccess files have Rewrite rules in them, treat them as described above.**

## Fork the project
On the [github enterprise] site, choose to forkthe repo.  This is where you will do all of your work.  Changes will be sent by pull request.

## Synchronize your local development
Go to where you want to do local development.

		> git clone git@github.ncsu.edu:username/project.git
		use this when the project already exists in git 
		
## Create a branch from master

		> git branch
		tells what branch you're on
		
		> git branch branchname
		creates the new branch
		> git checkout branchname
		switches to the branch
		
		> git checkout -b branchname
		creates the branch AND switches to it
		

## Initialize the project in Git



## Update the README.md in the project
There may be a README that can be copied.  Put a brief description of the project.  Include any of the necessary steps for configuration and install of the project.

## Review the issues with files that may or may not be used


